var mysql = require("mysql-await");

var connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "flight_ticket_booking",
});

exports.getListFlight = async function (from, to, time) {
    return await connection.awaitQuery(`SELECT * FROM flight WHERE flight.from = "${from}" AND flight.to = "${to}" AND flight.depart LIKE "${time}%"; `);
};

exports.getAllFlight = async function () {
    return await connection.awaitQuery(`SELECT * FROM flight;`);
};

exports.getAllSeat = async function () {
    return await connection.awaitQuery(`SELECT * FROM seat;`);
};

exports.getOneFlight = async function (id) {
    return await connection.awaitQuery(`SELECT * FROM flight WHERE id = "${id}";`);
};


// exports.getOneAccount = async function (phone) {
//     return await connection.awaitQuery(`SELECT * FROM (account JOIN client ON client.phone = account.phone) WHERE client.phone = ${phone};`);
// };

// exports.getListFlight = async function (phone) {
//     return await connection.awaitQuery(`SELECT * FROM (client_seat JOIN (seat JOIN (flight JOIN brand ON flight.brand= brand.id) ON seat.id_flight = flight.id) ON client_seat.id_seat = seat.id) WHERE phone=  ${phone};`);
// };


// exports.updateOne = async function (name, address, phone, email, sex, nationality) {
//     await connection.awaitQuery(
//         `UPDATE client SET name = "${name}", address = "${address}", email="${email}", sex="${sex}", nationality="${nationality}" WHERE phone = ${phone}; `
//     );
// };