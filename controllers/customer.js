var express = require("express");
var router = express.Router();
var customerModel = require("../models/customer");
const bcrypt = require('bcrypt');

router.get("/", async function (req, res) {
    const user = req.session.user;
    // console.log(user);
    let listAccount = await customerModel.getOneAccount(user.phone);
    let listFlight = await customerModel.getListFlight(user.phone);
    console.log(listAccount);
    var viewBag = {
        listAccount: listAccount,
        listFlight: listFlight,
    }
    res.render("customer/index", viewBag);
});

router.post("/edit", async function (req, res) {
    var name =  req.body.name;
    var address = req.body.address;
    var phone = req.body.phone;
    var email = req.body.email;
    var sex = req.body.sex;
    var nationality = req.body.nationality;
    // console.log(name, address);

    try {
        await customerModel.updateOne(name, address, phone, email, sex, nationality);
    } catch (err) {
        console.log(err);
        res.redirect("/500");
    }

    return res.redirect("/customer"); 
});

router.post("/", async function (req, res) {
    var password =  req.body.password_; //mat khau hien tai 
    var phone = req.body.phone;
    var pwd = req.body.password;
    var confirmPassword= req.body.confirmPassword;

    const user = req.session.user;
    // console.log(user);
    let listAccount = await customerModel.getOneAccount(user.phone);
    let listFlight = await customerModel.getListFlight(user.phone);
    console.log(listAccount);

    
    console.log(password);
    if (password){
        if (!bcrypt.compareSync(password, pwd)) {
            // console.log("login failed");
            var userInfo = { status: "failed password"}
            var viewBag = {
                listAccount: listAccount,
                listFlight: listFlight,
                data: userInfo
            }
            // var viewInfo = { data: userInfo }
            res.render("customer/index", viewBag);

        }
        else{
    // 

    await customerModel.updatePass(confirmPassword, phone);
    var userInfo = { status: "password"}
    var viewBag = {
        listAccount: listAccount,
        listFlight: listFlight,
        data: userInfo
    }
    // var viewInfo = { data: userInfo }
    // res.redirect("/customer", viewBag); 
    res.render("customer/index", viewBag);
  
    }
    res.redirect("/customer"); 
    }
  

});
// router.post("/listInfo", async function (req, res) {
//     let listAccount = await customerModel.getOneAccount("0300000000");
//     if(listAccount.length>0) {
//         res.json({
//             data: listAccount
//     })};
// });


module.exports = router;
